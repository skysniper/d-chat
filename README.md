# D-Chat

A decentralized chat on NKN as an extension for Chrome and Firefox.

https://addons.mozilla.org/en-US/firefox/addon/d-chat/

https://chrome.google.com/webstore/detail/glnmkakdjcognfgonjfcklpmjiobijii/

## Installation

`git submodule init --recursive`

`npm install`

`npm run dev`
or
`npm run build`
or
`npm start:firefox`
`npm start:chrome`

## Info

Forked from https://github.com/web3infra/dchat .

React broilerplate from https://github.com/kryptokinght/react-extension-boilerplate .

## Contributing

Send a merge request if you've got something nice, thanks! And make issues and so forth, basic stuff. Appreciate.

## Supporting with the monies

Always appreciated.

ERC20: 0x87107249Bf26A6c5CA9D55E05b7BF29aD50CD7C8

NKN coin (for mining rewards, for example): NKNaaUAAYNoZoDyRF3LoJLSsczo77vU38yz1

## License

MIT
