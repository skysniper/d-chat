/* global Configs */

export default new Configs({
	walletJSON: null,
	messages: {},
	showNotifications: true,
	chatSettings: {}
}, {
	localKeys: [ 'chatSettings', 'showNotifications' ]
});
